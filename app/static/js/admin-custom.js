
function import_receipt_check_all(){
    import_checks = document.getElementsByClassName("import_check")
    is_clicked = document.getElementById("is_clicked")
    is_clicked.checked = !is_clicked.checked

    Array.from(import_checks).forEach( item =>{
        item.checked = is_clicked.checked
    })

    if(is_clicked.checked)
        select_all.innerText = "Bỏ chọn tất cả"
    else
        select_all.innerText = "Chọn tất cả"
}

window.addEventListener("load", function(event) {
    import_date = document.getElementById("import_date")
    if (import_date != null){
        var today = new Date;
        var date = String(today.getDate()).padStart(2, '0');
        var month = String(today.getMonth() + 1).padStart(2, '0');
        var year = today.getFullYear();
        var strDate = year + "-" + month + "-" + date;
        import_date.value = strDate;
    }
});