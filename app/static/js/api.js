
function clear_all() {
    fetch("/client/api/temp-order/clear", {
        method: "delete"
    }).then(function(res) {
        return res.json();
    }).then(function(data) {
        console.info(data);
        Array.from(document.getElementsByClassName("temp_book_line")).forEach(t => t.style.display = "none")
        document.getElementById("total").innerText = "0 VNĐ"
    }).catch(function(err) {
        console.error(err);
    });
}

function debt_select(selected_debt){
    cus_cash_input = document.getElementById("cus_cash_input")
    customer_cash(cus_cash_input.value)

    fetch("/api/client/sale-order/debt/" + selected_debt, {
        method: "post"
    }).then(function(res) {
        return res.json();
    }).then(function(data) {
        console.info(data);

        debt_making = document.getElementById("debt_making")
        total = document.getElementById("total")

        debt_making.innerText = (data.data.debt_value + " VNĐ").replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        total.innerText = (data.data.total + " VNĐ").replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }).catch(function(err) {
        console.error(err);
    });
}

function customer_cash(cash){
    cus_cash_input = document.getElementById("cus_cash_input")
    debt_check = document.getElementById("debt_check").checked

    if(!debt_check){
        debt_options = 0
    }else{
        debt_options = document.getElementById("debt_options").value
    }

    if(cus_cash_input.value != ''){
        fetch("/api/client/sale-order/customer_cash/" + cash + "/" + debt_options, {
            method: "post"
        }).then(function(res) {
            return res.json();
        }).then(function(data) {
            console.info(data);

            cash_element = document.getElementById("cash")
            charge_element = document.getElementById("charge")

            cash_element.innerText = (data.data.cash + " VNĐ").replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
            charge_element.innerText = (data.data.charge + " VNĐ").replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }).catch(function(err) {
            console.error(err);
        });
    }
}

function customer_debt_orders(cus_id){
    fetch("/api/client/" + cus_id.value + "/debt-orders", {
        method: "post"
    }).then(function(res) {
        return res.json();
    }).then(function(data) {
        console.info(data);
        cus_orders = document.getElementById("cus_orders");
        if(data.data.length == 0){
            cus_orders.appendChild(document.createTextNode("Không tìm thấy hóa đơn ghi nợ nào!"));
        }
    }).catch(function(err) {
        console.error(err);
    });
}