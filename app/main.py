import hashlib
from pydoc import locate
from urllib.parse import unquote
from flask import flash, render_template, jsonify
from sqlalchemy import event
from app import login, admin, my_app, utils
from app.constant import *
from app.decorator import *
from app.models import *
from app.utils import total_calculator
from app.views import *


# Render HomePage
@my_app.route("/")
def index():
    return render_template("index.html")


# To Admin
@my_app.route("/admin")
def admin_index():
    return render_template("/admin/index.html")


# Admin Login
@my_app.route("/login-admin", methods=['GET', 'POST'])
def login_admin():
    if request.method == 'POST':
        username = request.form.get("manager_id")
        password = request.form.get("password", "")
        password = str(hashlib.md5(password.strip().encode("utf-8")).hexdigest())

        if not dao.admin_auth(username, password):
            flash("Mã nhân viên hoặc mật khẩu không đúng. Xin nhập lại!", category="error")
        else:
            if "next" in request.args:
                return redirect(request.args["next"])

    return redirect(url_for("admin_index"))


@login.user_loader
def load_admin(user_id):
    return Manager.query.get(user_id)


@event.listens_for(Employee.emp_password, 'set', retval=True)
def hash_user_password(target, value, old_value, initiator):
    if value != old_value:
        return str(hashlib.md5(value.strip().encode("utf-8")).hexdigest())
    return value


@my_app.route('/confirm')
def confirm():
    desc = request.args['desc']
    action_url = unquote(request.args['action_url'])

    return render_template('admin/confirm.html', desc=desc, action_url=action_url)


# Add views
admin.add_view(EmployeeModelView(Employee, db.session, name="Nhân viên"))

admin.add_view(BookModelView(Book, db.session, name="Đầu sách"))
admin.add_view(GenreModelView(Genre, db.session, name="Thể loại"))
admin.add_view(AuthorModelView(Author, db.session, name="Tác giả"))
admin.add_view(PublisherModelView(Publisher, db.session, name="Nhà xuất bản"))
admin.add_view(ImportReceiptModelView(ImportReceipt, db.session, name="Lập phiếu nhập sách"))
admin.add_view(ImportDetailModelView(ImportReceipt, db.session, name="Phiếu nhập sách", endpoint="import-receipt/list"))

# admin.add_view(CustomerModelView(Customer, db.session, name="Khách hàng"))
# admin.add_view(SaleOrderView(SaleOrder, db.session, name="Hoá đơn bán hàng"))
# admin.add_view(DebtMakingView(DebtMaking, db.session, name="Hoá đơn ghi nợ"))
# admin.add_view(PaymentBillView(PaymentBill, db.session, name="Phiếu thu tiền"))
#
admin.add_view(ReportModelView(StockReport, db.session, name="Báo cáo tồn"))
# admin.add_view(DebtReportModelView(DebtReport, db.session, name="Báo cáo công nợ"))
# admin.add_view(DebtReportDetailModelView(DebtReportDetail, db.session, name="Chi tiết công nợ"))
admin.add_view(RuleModelView(Rule, db.session, name="Qui định"))
admin.add_view(RuleDetailModelView(RuleDetail, db.session, name="Chi tiết thay đổi qui định"))

admin.add_view(LogoutView(name="Đăng xuất"))


# End add views

# HomePage function
# To Client
@my_app.route("/client")
def client_index():
    if utils.is_temp_order_created():
        # print(session[KEY_TEMP_ORDER])
        return render_template("client/index.html", title="Lập hoá đơn tạm",
                               books=session[KEY_TEMP_ORDER_SESSION], total=utils.total_calculator())

    return render_template("client/index.html", title="Lập hoá đơn tạm")


@my_app.route("/client", methods=['POST'])
@login_required
def add_book_to_temp_order():
    barcode = request.form.get("barcode")
    book = dao.get_book_by_barcode(barcode.strip())
    total = None
    if barcode and book:
        session[KEY_TEMP_ORDER_SESSION] = utils.write_temp_order(book)
        total = total_calculator()

    return render_template("client/index.html", title="Lập hoá đơn tạm",
                           books=session[KEY_TEMP_ORDER_SESSION], total=total)


@my_app.route("/client/remove/<barcode>", methods=['GET'])
def remove_temp_book(barcode):
    return render_template("client/index.html", title="Lập hoá đơn tạm",
                           books=utils.remove_temp_book(barcode), total=total_calculator())


@my_app.route("/client/quantity-modal/<barcode>", methods=['GET'])
def open_change_quantity_modal(barcode):
    book = None
    for temp_book in session[KEY_TEMP_ORDER_SESSION]:
        if temp_book["barcode"] == barcode:
            book = temp_book

    return render_template("client/quantity-modal.html", title="Lập hoá đơn tạm",
                           books=session[KEY_TEMP_ORDER_SESSION], book=book, total=total_calculator())


@my_app.route("/client/quantity-modal/<barcode>", methods=['POST'])
def change_quantity_modal(barcode):
    new_quantity = request.form.get("quantity_input")
    for idx, temp_book in enumerate(session[KEY_TEMP_ORDER_SESSION]):
        if temp_book["barcode"] == barcode:
            session[KEY_TEMP_ORDER_SESSION] = utils.change_quantity_temp_books(index=idx, quantity=int(new_quantity))
            return render_template("client/index.html", title="Lập hoá đơn tạm",
                                   books=session[KEY_TEMP_ORDER_SESSION],
                                   total=session[KEY_TEMP_ORDER_SESSION][idx]["price"])

    return redirect(url_for("client_index"))


@my_app.route("/client/api/temp-order/clear", methods=["DELETE"])
def clear_temp_order():
    if utils.clear_temp():
        return jsonify({
            "status": 200,
            "message": "Successful",
        })
    return jsonify({
        "status": 500,
        "message": "Failure",
    })


# End HomePage


# Create SaleOrder function
@my_app.route("/client/sale-order")
def render_sale_oder():
    # only effect if temp order is not empty
    if not utils.is_temp_order_created():
        err_msg = "Chưa có sản phẩm nào trên trang hoá đơn tạm"
        return render_template("client/result-modal.html", template="client/index.html",
                               title="Chi tiết hoá đơn", err_msg=err_msg, modal_title=KEY_ERROR_TITLE,
                               redirect_url="client_index")

    return render_template("client/create-order.html", title="Chi tiết hoá đơn",
                           books=session[KEY_TEMP_ORDER_SESSION], total=utils.total_calculator())


@my_app.route("/client/sale-order", methods=['POST'])
def commit_sale_order():
    customer_id = request.form.get("customer_input")
    customer = dao.get_customer_by_id(customer_id)
    if customer_id != '' and not customer:  # check customer if it's exist
        return sale_order_err_modal("Không tìm thấy mã khách hàng!")

    debt_option = request.form.get("debt_options")

    if not dao.save_sale_order(customer_id, debt_option):  # sale order's tran is failed
        return sale_order_err_modal("Server đang gặp sự cố!")

    if customer:
        cus_name = customer.customer_name
    else:
        cus_name = ""

    dao.print_bill(
        cus_name=cus_name,
        sale_order=dao.get_sale_order_by_id(session[KEY_PRINT_BILL_SESSION][0])
    )
    # clear temp
    session[KEY_PRINT_BILL_SESSION] = []
    utils.clear_temp()

    return render_template("client/result-modal.html", template="client/create-order.html",
                           title="Chi tiết hoá đơn", modal_title=KEY_SUCCESS_TITLE,
                           err_msg="Thanh toán thành công", books=session[KEY_TEMP_ORDER_SESSION],
                           redirect_url="client_index")


@my_app.route("/api/client/sale-order/debt/<selected_debt>", methods=['POST'])
def select_debt(selected_debt):
    if selected_debt:
        return jsonify({
            "status": 200,
            "message": "Successful",
            "data": {
                "debt_value": utils.debt_calculator(selected_debt),
                "total": utils.total_calculator() - utils.debt_calculator(selected_debt)
            }
        })
    return jsonify({
        "status": 500,
        "message": "Failure",
    })


@my_app.route("/api/client/sale-order/customer_cash/<cash>/<debt>", methods=['POST'])
def customer_cash(cash, debt):
    if cash and debt:
        return jsonify({
            "status": 200,
            "message": "Successful",
            "data": {
                "cash": float(cash),
                "charge": float(cash) - utils.total_calculator() + utils.debt_calculator(float(debt)),
            }
        })
    return jsonify({
        "status": 500,
        "message": "Failure",
    })


@my_app.route("/client/sale-order/result")
def sale_order_err_modal(err_msg):
    return render_template("client/result-modal.html", template="client/create-order.html",
                           title="Chi tiết hoá đơn", modal_title=KEY_ERROR_TITLE, err_msg=err_msg,
                           books=session[KEY_TEMP_ORDER_SESSION], redirect_url="render_sale_oder")


# End SaleOrder

# LookUp


@my_app.route("/client/lookup/by-name", methods=['GET', 'POST'])
def look_up_by_name():
    result = None
    books = None
    if request.method == 'POST':
        key_word = request.form.get("lookup_input")

        data = dao.lookup_by_key_name(key_word=key_word)
        result = data["result"]
        books = data["books"]
    return render_template("client/lookup.html", title="Tra cứu sách", result=result, books=books)


@my_app.route("/client/lookup/by-price", methods=['GET', 'POST'])
def look_up_by_price():
    result = None
    books = None
    if request.method == 'POST':
        from_price = request.form.get("lookup_input_from")
        to_price = request.form.get("lookup_input_to")

        data = dao.lookup_by_price(from_price, to_price)
        result = data["result"]
        books = data["books"]
    return render_template("client/lookup-by-price.html", title="Tra cứu sách", result=result, books=books)


@my_app.route("/client/lookup/by-genre", methods=['GET', 'POST'])
def look_up_by_genre():
    result = None
    books = None
    if request.method == 'POST':
        key_word = request.form.get("lookup_input")

        data = dao.lookup_by_key_genre(key_word=key_word)
        result = data["result"]
        books = data["books"]
    return render_template("client/lookup-by-genre.html", title="Tra cứu sách", result=result, books=books)


@my_app.route("/client/lookup/by-author", methods=['GET', 'POST'])
def look_up_by_author():
    result = None
    books = None
    if request.method == 'POST':
        key_word = request.form.get("lookup_input")

        data = dao.lookup_by_key_author(key_word=key_word)
        result = data["result"]
        books = data["books"]
    return render_template("client/lookup-by-author.html", title="Tra cứu sách", result=result, books=books)


@my_app.route("/client/lookup/by-publisher", methods=['GET', 'POST'])
def look_up_by_publisher():
    result = None
    books = None
    if request.method == 'POST':
        key_word = request.form.get("lookup_input")

        data = dao.lookup_by_key_publisher(key_word=key_word)
        result = data["result"]
        books = data["books"]
    return render_template("client/lookup-by-publisher.html", title="Tra cứu sách", result=result, books=books)


#
# @my_app.route("/api/lookup/get-books-names", methods=['POST'])
# def get_books_names():
#     names = dao.get_books_name_list()
#     if len(names) > 0:
#         return jsonify({
#             "status": 200,
#             "message": "Successful",
#             "books_name_list": names
#         })
#     return jsonify({
#         "status": 500,
#         "message": "Failure",
#     })


@my_app.route("/api/lookup/get-books-authors", methods=['POST'])
def get_books_authors():
    names = dao.get_author_name_list()
    if len(names) > 0:
        return jsonify({
            "status": 200,
            "message": "Successful",
            "books_name_list": names
        })
    return jsonify({
        "status": 500,
        "message": "Failure",
    })


@my_app.route("/api/lookup/get-books-genres", methods=['POST'])
def get_books_genres():
    names = dao.get_genre_name_list()
    if len(names) > 0:
        return jsonify({
            "status": 200,
            "message": "Successful",
            "books_name_list": names
        })
    return jsonify({
        "status": 500,
        "message": "Failure",
    })


@my_app.route("/api/lookup/get-books-publishers", methods=['POST'])
def get_books_publishers():
    names = dao.get_publisher_name_list()
    if len(names) > 0:
        return jsonify({
            "status": 200,
            "message": "Successful",
            "books_name_list": names
        })
    return jsonify({
        "status": 500,
        "message": "Failure",
    })


# Customer
@my_app.route("/client/customer/", methods=['GET'])
def render_customer():
    customer = None
    if KEY_CUSTOMER_SESSION in session:
        customer = session[KEY_CUSTOMER_SESSION]
        session[KEY_CUSTOMER_SESSION] = {}
    return render_template("client/customer.html", title="Quản lý khách hàng",
                           customer=customer)


@my_app.route("/client/customer/", methods=['POST'])
@login_required
def search_customer():
    search_bar = request.form.get("cus_search")

    if search_bar == "":  # search bar is empty
        err_msg = "Xin nhập mã khách hàng hoặc số điện thoại để tra cứu khách hàng!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    result = dao.check_customer_search_type(search_bar)

    if result is None:  # not found
        err_msg = "Không tìm thấy khách hàng!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    data = result["data"]
    search_type = result["type"]

    if search_type == "name":  # Search by name
        return render_template("client/customer.html", title="Quản lý khách hàng",
                               cus_list=data)  # Render customer list only

    if search_type == "id or phone":  # Search by id or phone
        return render_template("client/customer.html", title="Quản lý khách hàng", customer=data,
                               orders=dao.get_debt_order_by_customer(data))

    #  not in main flow or exception flow
    err_msg = "Server đang gặp sự cố!"
    return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                           modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")


@my_app.route("/client/customer/<cus_id>", methods=['GET', 'POST'])
def search_customer_by_name_list(cus_id):
    customer = dao.get_customer_by_id(cus_id)
    orders = dao.get_debt_order_by_customer(customer)
    return render_template("client/customer.html", title="Quản lý khách hàng", customer=customer,
                           orders=orders)


@my_app.route("/client/customer/clear", methods=['POST'])
def clear_fields():
    session[KEY_CUSTOMER_SESSION] = None
    redirect(url_for("render_customer.html"))


@my_app.route("/client/customer/add", methods=['POST'])
@login_required
def add_new_customer():
    cus_id = request.form.get("cus_id")
    cus_phone = request.form.get("cus_phone")
    cus_name = request.form.get("cus_name")
    cus_email = request.form.get("cus_email")
    cus_add = request.form.get("cus_address")

    session[KEY_CUSTOMER_SESSION] = Customer(
        customer_id=cus_id,
        customer_name=cus_name,
        customer_phone_number=cus_phone,
        customer_email=cus_email,
        customer_address=cus_add
    ).to_json()

    if cus_id == "" or cus_name == "" or cus_phone == "" or cus_add == "":
        err_msg = "Xin nhập thông tin cần thiết!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    if dao.get_customer_by_id(cus_id) is not None:
        err_msg = "Mã khách hàng đã tồn tại!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    if dao.get_customer_by_phone_number(cus_phone) is not None:
        err_msg = "Số điện thọai này đã có người đăng ký!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    if dao.save_new_customer(cus_id, cus_name, cus_phone, cus_email, cus_add):
        err_msg = "Thêm thành công!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_SUCCESS_TITLE, redirect_url="render_customer")

    err_msg = "Server đang gặp sự cố!"
    return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                           modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")


@my_app.route("/client/customer/update/", methods=['POST'])
@login_required
def update_customer():
    cus_id = request.form.get("cus_id")
    cus_phone = request.form.get("cus_phone")
    cus_name = request.form.get("cus_name")
    cus_email = request.form.get("cus_email")
    cus_add = request.form.get("cus_address")

    session[KEY_CUSTOMER_SESSION] = Customer(
        customer_id=cus_id,
        customer_name=cus_name,
        customer_phone_number=cus_phone,
        customer_email=cus_email,
        customer_address=cus_add
    ).to_json()

    if cus_id == "" or cus_name == "" or cus_phone == "" or cus_add == "" or cus_email == "":
        err_msg = "Xin không để trống ô nhập!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                                  modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    if dao.get_customer_by_id(cus_id) is None:
        err_msg = "Không tìm thấy khách hàng!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    if dao.is_phone_number_conflict(cus_id, cus_phone):
        err_msg = "Số điện thọai này đã có người sử !"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    if dao.update_customer(
            cus_id=cus_id,
            cus_name=cus_name,
            cus_phone=cus_phone,
            cus_email=cus_email,
            cus_add=cus_add):
        err_msg = "Cập nhật thành công!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_SUCCESS_TITLE, redirect_url="render_customer")

    err_msg = "Server đang gặp sự cố!"
    return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                           modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")


@my_app.route("/client/customer/delete/", methods=['POST'])
@login_required
def delete_customer():
    cus_id = request.form.get("cus_id")
    cus_phone = request.form.get("cus_phone")

    if cus_id:  # Search by id
        customer = dao.get_customer_by_id(cus_id)

    elif cus_phone:  # Search by phone number
        customer = dao.get_customer_by_phone_number(cus_phone)

    else:
        err_msg = "Xin nhập mã khách hàng hoặc số điện thoại để tra cứu khách hàng!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    if not customer:
        err_msg = "Không tìm thấy khách hàng!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    if not customer.delete():
        err_msg = "Server đang gặp sự cố!"
        return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                               modal_title=KEY_ERROR_TITLE, redirect_url="render_customer")

    err_msg = "Xóa thành công!"
    return render_template("client/result-modal.html", template="client/customer.html", err_msg=err_msg,
                           modal_title=KEY_SUCCESS_TITLE, redirect_url="render_customer")


# End customer

# Cash bill

@my_app.route("/client/cash-bill", methods=['GET'])
def render_cash_bill():
    rule = dao.get_rule_by_id(KEY_CASH_BILL_RULE)
    is_rule_using = False
    emp_session = None

    if KEY_CASH_BILL_SESSION in session:
        session[KEY_CASH_BILL_SESSION] = {}

    if KEY_USER_SESSION in session:
        emp_session = session[KEY_USER_SESSION]

    if rule.value_type == "bool" and rule.value == "1":
        is_rule_using = True

    return render_template("client/cash-bill.html", title="Phiếu thu tiền",
                           employee=emp_session, is_rule_using=is_rule_using)


@my_app.route("/client/cash-bill/search", methods=['POST'])
@login_required
def search_cus_cash_bill():
    rule = dao.get_rule_by_id(KEY_CASH_BILL_RULE)
    is_rule_using = False

    cus_id = request.form.get("cus_id")
    cus_cash = request.form.get("cus_cash")
    customer = dao.get_customer_by_id(cus_id)

    if rule.value_type == "bool" and rule.value == "1":
        is_rule_using = True

    session[KEY_CASH_BILL_SESSION] = {
        "cus_id": cus_id,
        "cus_cash": cus_cash,
        "is_rule_using": is_rule_using
    }

    if customer is None:
        err_msg = "Không tìm thấy khách hàng!"
        return render_template("client/result-modal.html", template="client/cash-bill.html",
                               err_msg=err_msg, modal_title=KEY_ERROR_TITLE,
                               redirect_url="render_cash_bill")

    all_debt_orders = utils.total_debt_calculator(customer.orders)

    return render_template("client/cash-bill.html", title="Phiếu thu tiền",
                           employee=session[KEY_USER_SESSION], is_rule_using=is_rule_using,
                           customer=customer, all_debt_orders=all_debt_orders,
                           session=session[KEY_CASH_BILL_SESSION])


@my_app.route("/client/cash-bill/save", methods=['POST'])
@login_required
def save_and_print_cash_bill():
    cus_id = request.form.get("cus_id")
    cus_cash = request.form.get("cus_cash")
    customer = dao.get_customer_by_id(cus_id)
    rule = dao.get_rule_by_id(KEY_CASH_BILL_RULE)
    is_rule_using = False

    if rule.value_type == "bool" and rule.value == "1":
        is_rule_using = True

    if cus_cash == "":
        err_msg = "Chưa nhập nhập số tiền thu!"
        return render_template("client/result-modal.html", title="Phiếu thu tiền",
                               template="client/cash-bill.html", is_rule_using=is_rule_using,
                               err_msg=err_msg, modal_title=KEY_ERROR_TITLE,
                               redirect_url="render_cash_bill")

    if customer is None:
        err_msg = "Không tìm thấy khách hàng!"
        return render_template("client/result-modal.html", title="Phiếu thu tiền",
                               template="client/cash-bill.html", is_rule_using=is_rule_using,
                               err_msg=err_msg, modal_title=KEY_ERROR_TITLE,
                               redirect_url="render_cash_bill")

    cus_cash = float(cus_cash)
    if is_rule_using and cus_cash > utils.total_debt_calculator(customer.orders):
        err_msg = "Quy định 5 đang được áp: Số tiền thu không vượt quá số tiền khách hàng đang nợ !"
        return render_template("client/result-modal.html", title="Phiếu thu tiền",
                               template="client/cash-bill.html",
                               err_msg=err_msg, modal_title=KEY_ERROR_TITLE,
                               redirect_url="render_cash_bill", is_rule_using=is_rule_using)

    new_cash_bill = dao.save_new_cash_bill(
        cash=cus_cash,
        customer_id=customer.customer_id,
        creator_id=session[KEY_USER_SESSION]["id"])

    if new_cash_bill:
        err_msg = "Lưu thành công!"
        return render_template("client/result-modal.html", template="client/cash-bill.html", err_msg=err_msg,
                               modal_title=KEY_SUCCESS_TITLE, redirect_url="render_cash_bill"
                               , is_rule_using=is_rule_using)

    err_msg = "Server đang gặp sự cố!"
    return render_template("client/result-modal.html", template="client/cash-bill.html", err_msg=err_msg,
                           modal_title=KEY_ERROR_TITLE, redirect_url="render_cash_bill", is_rule_using=is_rule_using)


# End cash bill


# Client Authentication
@my_app.route("/client/login", methods=['GET', 'POST'])
def login_client():
    err_msg = ""
    employee_id = ""
    if request.method == "POST":
        employee_id = request.form.get("employee_id")
        password = request.form.get("password")
        password = str(hashlib.md5(password.strip().encode("utf-8")).hexdigest())

        employee = dao.client_auth(employee_id=employee_id, password=password)
        if employee:
            session[KEY_USER_SESSION] = employee.to_json()

            if "next" in request.args:
                return redirect(request.args["next"])

            return redirect(url_for("client_index"))
        else:
            err_msg = "Mã nhân viên hoặc mật khẩu không đúng!"

    return render_template("client/login.html", err_msg=err_msg, employee_id=employee_id)


@my_app.route("/client/logout")
def logout_client():
    session[KEY_USER_SESSION] = None
    clear_temp_order()
    return redirect(url_for("client_index"))


# End Authentication

# Press the green button in the gutter to run the script.


if __name__ == '__main__':
    my_app.run(debug=True, port=5000)
