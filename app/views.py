from flask import redirect, request, flash, url_for, session
from flask_admin import BaseView, expose
from flask_admin.contrib.sqla import ModelView, fields
from jinja2.runtime import Context
from sqlalchemy import func
from sqlalchemy.orm import Session
from wtforms import PasswordField

from app import dao, db
from app.constant import KEY_IMPORT_SESSION, KEY_IMPORT_QUAN_RULE, KEY_IMPORT_IN_STOCK_RULE
from app.decorator import confirmation_required
from app.models import ImportReceipt, ImportDetail, Book
from datetime import datetime


class AuthenticatedView(ModelView):
    # edit_template = "admin/custom-style.html"

    @expose('/new/', methods=('GET', 'POST'))
    def create_view(self):
        if not dao.is_accessible():
            return redirect("/admin")
        return super(AuthenticatedView, self).create_view()

    @expose('/edit/', methods=('GET', 'POST'))
    def edit_view(self):
        if not dao.is_accessible():
            return redirect("/admin")
        return super(AuthenticatedView, self).edit_view()

    @expose('/delete/', methods=('GET', 'POST'))
    def delete_view(self):
        if not dao.is_accessible():
            return redirect("/admin")
        return super(AuthenticatedView, self).delete_view()


class ModalAuthenticatedView(AuthenticatedView):
    edit_modal = True
    create_modal = True
    details_modal = True


class EmployeeModelView(AuthenticatedView):
    column_display_pk = True
    column_labels = dict(
        id='Mã nhân viên',
        emp_password='Mật khẩu (Đã mã hóa)',
        emp_name='Tên nhân viên',
        emp_birth='Ngày sinh',
        emp_phone_number='Số điên thoại',
        emp_address='Địa chỉ',
        is_manager='Chức vụ quản lý',
        sale_orders='Hoá đơn',
    )

    column_list = ('id',
                   'emp_name',
                   'emp_birth',
                   'emp_phone_number',
                   'emp_address',
                   'is_manager',
                   'sale_orders',)

    form_columns = ('id', 'emp_password', 'emp_name', 'emp_birth', 'emp_phone_number',
                    'emp_address',)


class BookModelView(AuthenticatedView):
    column_display_pk = True
    can_export = True
    column_labels = dict(
        barcode='Mã vạch',
        book_name='Tên sách',
        book_description='Mô tả',
        book_price='Giá niêm yết',
        in_stock_quantity="Số lượng tồn kho",
        genres="Thể loại",
        authors="Tác giả",
        publishers="Nhà xuất bản",
        thumbnail="Bìa sách",
        # import_receipt="Phiếu nhập",
        # sale_orders="Hoá đơn",
        # reports="Báo cáo tồn",
    )
    column_list = (
        'barcode',
        'book_name',
        'book_description',
        'book_price',
        'in_stock_quantity',
        'genres',
        'authors',
        'publishers',
        'thumbnail',
        # 'import_receipt',
        # 'sale_orders',
        # 'reports',
    )

    form_columns = ('barcode', 'book_name', 'book_description', 'book_price', 'in_stock_quantity',
                    'genres', 'authors', 'publishers', 'thumbnail',)

    # def get_query(self):
    #     return self.session.query(self.model).filter(self.model.barcode == True)
    #
    # def get_count_query(self):
    #     return self.session.query(func.count('*')).filter(self.model.barcode == True)


class GenreModelView(ModalAuthenticatedView):
    column_display_pk = False
    column_labels = dict(
        genre_detail='Thể loại sách',
        books='Đầu sách',
    )

    column_list = ('genre_detail', 'books',)
    form_columns = ('genre_detail',)


class AuthorModelView(ModalAuthenticatedView):
    column_display_pk = False
    column_labels = dict(
        author_name='Tên tác giả',
        author_other_info='Thông tin chung',
        books='Các đầu sách',
    )

    column_list = ('author_name', 'author_other_info', 'books',)
    form_columns = ('author_name', 'author_other_info',)


class PublisherModelView(ModalAuthenticatedView):
    column_display_pk = False
    column_labels = dict(
        publisher_name='Tên nhà xuất bản',
        publisher_phone_number='Số điện thoại',
        publisher_address='Địa chỉ',
        books='Các đầu sách',
    )

    column_list = ('publisher_name', 'publisher_phone_number', 'publisher_address', 'books',)
    form_columns = ('publisher_name', 'publisher_phone_number', 'publisher_address',)


class InStockReportModelView(AuthenticatedView):
    column_display_pk = True
    can_export = True
    column_auto_select_related = True
    column_list = ('rp_id', 'created', 'creator_id', 'books', 'details')

    column_labels = dict(rp_id='Mã báo cáo',
                         created='Thời gian lập báo cáo',
                         updated='Thời gian cập nhật',
                         creator_id='Người lập báo cáo',
                         books='Đầu sách',
                         details='Chi tiết tồn', )

    form_columns = ('created', 'updated', 'creator_id', 'books',)


class InStockReportModelDetailView(AuthenticatedView):
    column_display_pk = True
    can_create = False
    column_list = ('rp_id', 'barcode', 'opening_stock', 'in_stock_incurred', 'closing_stock',)

    column_labels = dict(rp_id='Mã báo cáo',
                         barcode='Đầu sách',
                         opening_stock='Tồn đầu',
                         in_stock_incurred='Phát sinh',
                         closing_stock='Tồn cuối', )
    form_columns = ('rp_id', 'barcode', 'opening_stock', 'in_stock_incurred', 'closing_stock',)


class DebtReportModelView(AuthenticatedView):
    column_display_pk = True
    can_export = True
    can_edit = False
    can_delete = False
    column_list = ('rp_id', 'created', 'updated', 'creator_id', 'debt_receipts', 'details',)
    column_labels = dict(rp_id='Mã báo cáo',
                         created='Thời gian lập báo cáo',
                         updated='Thời gian lập báo cáo',
                         creator_id='Người báo cáo',
                         debt_receipts='Phiếu ghi nợ',
                         details='Chi tiết công nợ', )


class DebtReportDetailModelView(AuthenticatedView):
    column_display_pk = True
    can_create = False
    column_auto_select_related = True
    column_list = ('rp_id', 'debt_id', 'opening_stock', 'in_stock_incurred', 'closing_stock')

    column_labels = dict(rp_id='Mã báo cáo',
                         debt_id='Phiếu ghi nợ',
                         opening_stock='Tồn đầu',
                         in_stock_incurred='Phát sinh',
                         closing_stock='Tồn cuối', )
    form_columns = ('rp_id', 'debt_id', 'opening_stock', 'in_stock_incurred', 'closing_stock',)


class RuleModelView(ModelView):
    @expose('/', methods=['GET', 'POST'])
    def __index__(self):
        if not dao.is_accessible():
            return redirect("/admin")

        rules = dao.get_all_rules()
        if request.method == 'POST':
            for rule in rules:
                if rule.value_type == "bool":
                    if rule.rule_id in request.form:
                        value = "1"
                    else:
                        value = "0"
                else:
                    value = request.form.get(rule.rule_id)

                dao.update_rule(rule.rule_id, str(value))
            flash("Đã lưu thay đổi")

        return self.render("admin/edit_rule.html", rules=rules)


class RuleDetailModelView(AuthenticatedView):
    column_display_pk = True
    can_create = False
    can_edit = False
    can_delete = False
    column_labels = dict(rule_id='Mã qui  định',
                         changer_id='Người thay đổi',
                         change_date='Thời gian thay đổi')
    column_list = ('rule_id', 'changer_id', 'change_date',)


class ImportReceiptModelView(ModelView):
    @expose('/', methods=['GET', 'POST'])
    def __index__(self):
        rule_value = dao.get_rule_by_id(KEY_IMPORT_IN_STOCK_RULE).value  # book in stock import rule value
        books = dao.get_book_by_rule_value(rule_value)
        selected_books = request.form.getlist("import_check")

        if request.method == 'POST':
            if len(selected_books) > 0:
                session[KEY_IMPORT_SESSION] = selected_books
                return redirect(url_for("importreceipt.create_import"))
            else:
                flash("Bạn chưa chọn đầu sách nào!", category="error")

        return self.render("admin/import-receipt.html", rule_value=rule_value, books=books)

    @expose('/create-new-import/', methods=['GET', 'POST'])
    def create_import(self):
        import_date = request.form.get("import_date")
        rule_value = dao.get_rule_by_id(KEY_IMPORT_QUAN_RULE).value  # book import quantity rule value
        books_json = []
        for barcode in session[KEY_IMPORT_SESSION]:
            books_json.append({
                "book": dao.get_book_by_barcode(barcode),
                "import_quantity": request.form.get(barcode + "input")
            })

        if request.method == 'POST':
            new_import = dao.create_new_import(import_date)
            if new_import["result"]:
                for book in books_json:
                    dao.save_import_detail(
                        import_id=new_import["data"].import_id,
                        barcode=book["book"].barcode,
                        pre_quantity=book["book"].in_stock_quantity,
                        import_quantity=book["import_quantity"]
                    )

                dao.print_import_receipt(new_import["data"])
                flash("Thành công")
                return redirect(url_for("importreceipt.__index__"))

            else:
                flash("Lỗi! Server đang gặp sự cố", category="error")
        return self.render("admin/create-new-import.html", rule_value=rule_value, books=books_json)

    # After create import
    @db.event.listens_for(ImportDetail, "after_insert")
    def update_stock_quantity_after(mapper, connection, import_detail):
        import_quantity = import_detail.import_quantity

        @db.event.listens_for(Session, "after_flush", once=True)
        def receive_after_flush(db_session, context):
            connection.execute(
                Book.__table__
                    .update()
                    .values(in_stock_quantity=Book.in_stock_quantity + import_quantity)
                    .where(Book.barcode == import_detail.barcode)
            )


class ImportDetailModelView(AuthenticatedView):
    column_display_pk = True
    can_create = False
    can_edit = False
    can_delete = False
    column_labels = dict(import_id='Mã phiếu nhập',
                         import_date='Ngày nhập sách',
                         created='Ngày lập phiếu',
                         details='Chi tiết nhập', )
    column_list = ('import_id', 'import_date', 'created', 'details',)


class ReportModelView(ModelView):
    @expose('/', methods=['GET'])
    def __index__(self):
        year = datetime.now().year
        month = datetime.now().month
        months_in_year = ["12/" + str(year - 1)]

        for i in range(1, 12):
            if i < month:
                months_in_year.append(str(i) + "/" + str(year))

        import_of_month = []
        imports = dao.get_all_import_receipt()

        # opening_stock
        for i in imports:
            if i.import_date == month:
                import_of_month.append(i)

        return self.render("admin/report.html", year=year, months=months_in_year)

    @expose('/<month>', methods=['POST'])
    def month_select(self, month):
        # month = request.form.get("month_option")
        import_receipts = dao.get_all_import_receipt()
        result = []
        for receipt in import_receipts:
            import_month = datetime.strptime(receipt.import_date)
            if import_month.month < month:
                result.append(receipt)

        return redirect(url_for("stockreport.__index__"))


class LogoutView(BaseView):
    @expose("/")
    @confirmation_required("Bạn có chắc muốn đăng xuất")
    def index(self):
        dao.logout()
        return redirect("/admin")

    def is_accessible(self):
        return dao.is_accessible()
