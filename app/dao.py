import csv

from flask import session
from flask_login import login_user, logout_user, current_user
from app import utils
from app.constant import *
from app.models import *
from sqlalchemy.orm import Session


def print_bill(cus_name, sale_order):
    fieldnames = ["Họ tên khách hàng", "Ngày lập hóa đơn", "Sách", "Thể Loại", "Số lượng", "Đơn giá"]
    data = []
    for order in sale_order.details:
        book = get_book_by_barcode(order.barcode)
        genre = ""

        for idx, g in enumerate(book.genres):
            genre += g.genre_detail
            if idx != 0:
                genre += "-"

        data.append({
            "Họ tên khách hàng": cus_name,
            "Ngày lập hóa đơn": sale_order.created,
            "Sách": book.book_name,
            "Thể Loại": genre,
            "Số lượng": order.sell_quantity,
            "Đơn giá": book.book_price
        })
    return utils.export_csv(data, "sale_order", fieldnames)


def print_import_receipt(import_receipt):
    fieldnames = ["Ngày nhập", "Sách", "Thể Loại", "Tác giả", "Số lượng"]
    data = []
    for receipt in import_receipt.details:
        book = get_book_by_barcode(receipt.barcode)
        genre = ""
        author = ""

        for idx, g in enumerate(book.genres):
            genre += g.genre_detail
            if idx != 0:
                genre += ", "

        for idx, a in enumerate(book.authors):
            author += a.author_name
            if idx != 0:
                author += ", "

        data.append({
            "Ngày nhập": import_receipt.import_date,
            "Sách": book.book_name,
            "Thể Loại": genre,
            "Tác giả": author,
            "Số lượng": receipt.import_quantity
        })
    return utils.export_csv(data, "import_receipt", fieldnames)


# Read Database
def get_all_book():
    return Book.query.all()


def get_book_by_barcode(barcode):
    return Book.query.get(barcode)


def get_all_books_by_genre(key_word):
    return Book.query.filter(Book.genres.any(genre_detail=key_word)).all()


def get_all_books_by_author(key_word):
    return Book.query.filter(Book.authors.any(author_name=key_word)).all()


def get_all_books_by_publisher(key_word):
    return Book.query.filter(Book.publishers.any(publisher_name=key_word)).all()


def get_book_by_rule_value(rule_value):
    return Book.query.filter(Book.in_stock_quantity.__lt__(rule_value)).all()


def get_all_authors():
    return Author.query.all()


def get_all_genres():
    return Genre.query.all()


def get_all_publishers():
    return Publisher.query.all()


def get_all_rules():
    return Rule.query.all()


def get_rule_by_id(rule_id):
    return Rule.query.get(rule_id)


def get_sale_order_by_id(id):
    return SaleOrder.query.get(id)


def get_all_customer():
    return Customer.query.all()


def get_customer_by_id(cus_id):
    return Customer.query.get(cus_id)


def get_customer_by_phone_number(phone):
    return Customer.query.filter(Customer.customer_phone_number == phone).first()


def get_customer_by_name(name):
    return Customer.query.filter(Customer.customer_name == name).all()


def is_phone_number_conflict(cus_id, phone_number):
    result = []
    for cus in get_all_customer():
        if cus_id != cus.customer_id and cus.customer_phone_number == phone_number:
            result.append(cus)

    if len(result) > 0:
        return True
    return False


def get_all_import_receipt():
    return ImportReceipt.query.all()


# def get_import_by_month(month):
#     return ImportReceipt.query.filter(
#         datetime.strptime(str(ImportReceipt.import_date), '%y-%m-%d').month == month
#     ).all()


def check_customer_search_type(key_word):
    if get_customer_by_name(key_word):  # check by name
        return {
            "type": "name",
            "data": get_customer_by_name(key_word)
        }

    data = get_customer_by_id(key_word)

    if not data:  # check by id
        data = get_customer_by_phone_number(key_word)
        if not data:  # check by phone
            return None  # not found

    return {
        "type": "id or phone",
        "data": data
    }


def update_rule(rule_id, value):
    try:
        Rule.query.filter_by(rule_id=rule_id).update(dict(value=value))
        db.session.commit()
        return True
    except Exception as ex:
        print(ex)
    return False


def get_author_name_list():
    names = []
    authors = get_all_authors()
    for author in authors:
        names.append(author.author_name)
    return names


def get_genre_name_list():
    names = []
    genres = get_all_genres()
    for genre in genres:
        names.append(genre.genre_detail)
    return names


def get_publisher_name_list():
    names = []
    publishers = get_all_publishers()
    for publisher in publishers:
        names.append(publisher.publisher_name)
    return names


def get_books_by_name(key_word):
    return Book.query.filter(Book.book_name.like("%" + key_word + "%")).all()


def get_book_by_eq_price(price):
    return Book.query.filter(Book.book_price.__eq__(price)).all()


def get_books_by_price(from_price, to_price):
    return Book.query.filter(Book.book_price.__gt__(from_price), Book.book_price.__lt__(to_price)).all()


# End read


# Admin
def admin_auth(username="", password=""):
    user = Manager.query.filter(Manager.id == username, Manager.emp_password == password).first()
    if user:
        login_user(user=user)
        return True
    return False


def logout():
    logout_user()


def is_accessible():
    return current_user.is_authenticated


# Client
def client_auth(employee_id, password):
    return Employee.query.filter(Employee.id == employee_id, Employee.emp_password == password).first()


def save_sale_order(customer_id, debt):
    sale_order = SaleOrder()

    if customer_id != '':  # save anonymous order if customer's id is empty
        sale_order.customer_id = customer_id

    if debt:  # make debt if it's not none
        sale_order.debt_value = utils.debt_calculator(debt)

    sale_order.created = datetime.now()
    sale_order.creator_id = session[KEY_USER_SESSION]["id"]
    sale_order.total = utils.total_calculator()

    if not sale_order.insert():
        return False

    return True


def lookup_by_key_name(key_word):
    books = get_books_by_name(key_word)

    if len(books) > 0:
        return {
            "books": books,
            "result": "cho tên đầu sách có chứa từ khóa: " + key_word
        }

    return {
        "books": None,
        "result": ": Không tìm thấy đầu sách khớp với từ khóa: " + key_word
    }


def lookup_by_price(from_price, to_price):
    from_price = float(from_price)
    to_price = float(to_price)

    books = get_books_by_price(from_price, to_price)  # greater than or little than
    books.extend(get_book_by_eq_price(from_price))  # equal from
    books.extend(get_book_by_eq_price(to_price))  # equal to

    if len(books) > 0:
        return {
            "books": books,
            "result": "cho giá sách từ " + "{:,.0f}".format(from_price) + " đến " + "{:,.0f}".format(to_price)
        }

    return {
        "books": None,
        "result": ": Không tìm thấy đầu sách nào trong khoảng giá này"
    }


def lookup_by_key_genre(key_word):
    books = get_all_books_by_genre(key_word)

    if len(books) > 0:
        return {
            "books": books,
            "result": "cho đầu sách thuộc thể loại: " + key_word
        }

    return {
        "books": None,
        "result": ": Không tìm thấy đầu sách khớp với từ khóa: " + key_word
    }


def lookup_by_key_author(key_word):
    books = get_all_books_by_author(key_word)

    if len(books) > 0:
        return {
            "books": books,
            "result": "đầu sách của tác giả: " + key_word
        }

    return {
        "books": None,
        "result": ": Không tìm thấy đầu sách khớp với từ khóa: " + key_word
    }


def lookup_by_key_publisher(key_word):
    books = get_all_books_by_publisher(key_word)

    if len(books) > 0:
        return {
            "books": books,
            "result": "đầu sách của nhà xuất bản: " + key_word
        }

    return {
        "books": None,
        "result": ": Không tìm thấy đầu sách khớp với từ khóa: " + key_word
    }


def create_new_import(import_date):
    new_import = ImportReceipt()

    if import_date != "":
        new_import.import_date = import_date

    return {
        "result": new_import.insert(),
        "data": new_import
    }


def save_import_detail(import_id, barcode, pre_quantity, import_quantity):
    import_detail = ImportDetail(
        import_id=import_id,
        barcode=barcode,
        pre_quantity=pre_quantity,
        import_quantity=import_quantity
    )
    return import_detail.insert()


def get_debt_order_by_customer(customer):
    debt_orders = []
    for order in customer.orders:
        if order.debt_value > 0.0:
            debt_orders.append(order)
    return debt_orders


def save_new_customer(cus_id, cus_name, cus_phone, cus_email, cus_add):
    customer = Customer(
        customer_id=cus_id,
        customer_name=cus_name,
        customer_phone_number=cus_phone,
        customer_email=cus_email,
        customer_address=cus_add
    )
    return customer.insert()


def update_customer(cus_id, cus_name, cus_phone, cus_email, cus_add):
    try:
        Customer.query.filter_by(customer_id=cus_id).update(dict(
            customer_name=cus_name,
            customer_phone_number=cus_phone,
            customer_email=cus_email,
            customer_address=cus_add
        ))
        db.session.commit()
        return True
    except Exception as ex:
        print(ex)
    return False


def save_new_cash_bill(cash, customer_id, creator_id):
    new_cash_bill = CashBill()
    new_cash_bill.cash = cash
    new_cash_bill.customer_id = customer_id
    new_cash_bill.creator_id = creator_id

    return new_cash_bill.insert()


# Listener
# After save sale order
@db.event.listens_for(SaleOrder, "after_insert")
def create_order_detail_after(mapper, connection, sale_order):
    session[KEY_PRINT_BILL_SESSION] = []
    session[KEY_PRINT_BILL_SESSION].append(sale_order.order_id)

    @db.event.listens_for(Session, "after_flush", once=True)
    def receive_after_flush(db_session, context):
        for temp_book in session[KEY_TEMP_ORDER_SESSION]:
            db_session.add(OrderDetail(
                order_id=sale_order.order_id,
                barcode=temp_book["barcode"],
                sell_quantity=temp_book["quantity"],
                unit_price=temp_book["unit_price"]
            ))


# Update stock quantity after save order detail
@db.event.listens_for(OrderDetail, "after_insert")
def update_in_stock_quantity(mapper, connection, order_detail):
    book = get_book_by_barcode(order_detail.barcode)
    new_quantity = book.in_stock_quantity - order_detail.sell_quantity

    @db.event.listens_for(Session, "after_flush", once=True)
    def receive_after_flush(db_session, context):
        if not db_session.is_modified(order_detail, include_collections=False):
            return

        connection.execute(
            Book.__table__
                .update()
                .values(in_stock_quantity=new_quantity)
                .where(Book.barcode == order_detail.barcode)
        )


# Update debt after add new cash bill
@db.event.listens_for(CashBill, "after_insert")
def update_debt_value(mapper, connection, cash_bill):
    @db.event.listens_for(Session, "after_flush", once=True)
    def receive_after_flush(db_session, context):
        if not db_session.is_modified(cash_bill, include_collections=False):
            return

        connection.execute(
            SaleOrder.__table__
                .update()
                .values(debt_value=SaleOrder.debt_value - cash_bill)
                .where(SaleOrder.customer_id == cash_bill.customer_id)
        )

# # After change rule
# @db.event.listens_for(Rule, "after_update")
# def create_rule_detail_after(mapper, connection, rule):
#     @db.event.listens_for(Session, "after_flush", once=True)
#     def receive_after_flush(db_session, context):
#         db_session.add(RuleDetail(rule_id=rule.rule_id, changer_id=current_user.id))

# End listener
