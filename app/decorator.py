from functools import wraps
from urllib.parse import quote
from flask import session, redirect, url_for, request

from app import dao


def confirmation_required(message):
    def inner(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if request.args.get('confirm') != '1':
                desc = message
                return redirect(url_for('confirm', desc=desc, action_url=quote(request.url)))
            return f(*args, **kwargs)

        return wrapper

    return inner


def login_required(f):
    @wraps(f)
    def check(*args, **kwargs):
        if not session.get("user"):
            return redirect(url_for("login_client", next=request.url))

        return f(*args, **kwargs)

    return check
