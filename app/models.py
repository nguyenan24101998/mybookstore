import enum
from dataclasses import dataclass
from datetime import datetime
from flask_login import UserMixin
from sqlalchemy import Column, String, Integer, Float, Date, DateTime, ForeignKey, Boolean, Enum
from sqlalchemy.orm import relationship, backref
from app import db


# class FilterEqual(BaseSQLAFilter):
#     def apply(self, query, value, alias=None):
#         return query.filter(self.get_column(alias) == value)
#
#     def operation(self):
#         return lazy_gettext('equals')
#
#
# class FilterNotEqual(BaseSQLAFilter):
#     def apply(self, query, value, alias=None):
#         return query.filter(self.get_column(alias) != value)
#
#     def operation(self):
#         return lazy_gettext('not equal')
#
#
# class FilterLike(BaseSQLAFilter):
#     def apply(self, query, value, alias=None):
#         stmt = tools.parse_like_term(value)
#         return query.filter(self.get_column(alias).ilike(stmt))
#
#     def operation(self):
#         return lazy_gettext('contains')
#
#
# class FilterNotLike(BaseSQLAFilter):
#     def apply(self, query, value, alias=None):
#         stmt = tools.parse_like_term(value)
#         return query.filter(~self.get_column(alias).ilike(stmt))
#
#     def operation(self):
#         return lazy_gettext('not contains')
#

class Employee(db.Model):
    __tablename__ = "employee"

    id = Column(String(50), primary_key=True)
    emp_password = Column(String(50), nullable=False)
    emp_name = Column(String(50), nullable=False)
    emp_birth = Column(Date, nullable=True)
    emp_phone_number = Column(String(50), nullable=False)
    emp_address = Column(String(50), nullable=False)
    is_manager = Column(Boolean, nullable=False, default=False)

    manager = relationship('Manager', uselist=False, backref='employee')  # child

    sale_orders = relationship('SaleOrder', backref='creator', lazy=True)

    def to_json(self):
        return {
            "id": self.id,
            "name": self.emp_name,
            "birth": self.emp_birth,
            "phone": self.emp_phone_number,
            "address": self.emp_address,
            "is_manager": self.is_manager,
        }

    def __str__(self):
        return self.emp_name


class Manager(Employee, UserMixin):
    __tablename__ = "manager"

    id = Column(String(50), ForeignKey(Employee.id), primary_key=True)  # parent's id
    active = Column(Boolean, default=True)
    change_rules = relationship('RuleDetail', backref='changer', lazy=True)
    reports = relationship('Report', backref="creator", lazy=True)

    # in_stock_reports = relationship('InStockReport', backref="creator", lazy=True)  # many relate to Report
    # debt_reports = relationship('DebtReport', backref="manager", lazy=True)  # many relate to Report
    # book_manage = relationship('BookManagement', backref='managers', lazy=True)

    def __str__(self):
        super.__str__(self)


@dataclass
class Rule(db.Model):
    __tablename__ = "rule"

    rule_id = Column(String(50), primary_key=True)
    description = Column(String(255), nullable=False)
    value_type = Column(String(255), nullable=False)
    value = Column(String(50), nullable=False)

    details = relationship('RuleDetail', backref='rule', lazy=True)

    def __str__(self):
        return self.description


# # rule-manager's joining table
class RuleDetail(db.Model):
    __tablename__ = "change_rule"

    id = Column(Integer, primary_key=True, autoincrement=True)
    rule_id = Column(String(50), ForeignKey(Rule.rule_id))
    changer_id = Column(String(50), ForeignKey(Manager.id))
    change_date = Column(DateTime, nullable=False, default=datetime.now())

    def __init__(self, rule_id, changer_id):
        self.rule_id = rule_id
        self.changer_id = changer_id

    def __str__(self):
        return "Quy định mã " + self.rule_id

    def insert(self):
        try:
            db.session.add(self)
            db.session.commit()  # Apply transaction
            return True
        except Exception as ex:
            print(ex)
            db.session.rollback()  # Cancel transaction
        return False


# book-manager's joining table
# class BookManagement(db.Model):
#     barcode = Column(String(50), ForeignKey('book.barcode'), primary_key=True),
#     emp_id = Column(String(50), ForeignKey('manager.id'), primary_key=True)
#     # Column('added_date', DateTime, nullable=False),
#     # Column('edited_date', DateTime, nullable=True)

@dataclass
class Book(db.Model):

    __tablename__ = "book"

    barcode = Column(String(50), primary_key=True, autoincrement=False)
    book_name = Column(String(255), nullable=False)
    book_description = Column(String(500), nullable=True)
    book_price = Column(Float, nullable=False)
    in_stock_quantity = Column(Integer, nullable=False)
    thumbnail = Column(String(255), nullable=False)

    # reports = relationship('InStockReportDetail', backref='book', lazy=True)
    sale_orders = relationship('OrderDetail', backref='book', lazy=True)
    import_receipt = relationship('ImportDetail', backref='book', lazy=True)

    genres = relationship('Genre', secondary='book_genre_detail', lazy='subquery',
                          backref=backref('books', lazy=True))
    authors = relationship('Author', secondary='author_detail', lazy='subquery',
                           backref=backref('books', lazy=True))
    publishers = relationship('Publisher', secondary='publisher_detail', lazy='subquery',
                              backref=backref('books', lazy=True))

    stock_reports = relationship('StockReport', backref="books", lazy=True)

    def __str__(self):
        return self.book_name

    def to_json(self):
        return {
            "barcode": self.barcode,
            "name": self.book_name,
            "unit_price": self.book_price,
            "quantity": 1,
            "price": self.book_price
        }


@dataclass
class Genre(db.Model):
    __tablename__ = "book_genre"

    genre_id = Column(Integer, primary_key=True, autoincrement=True)
    genre_detail = Column(String(50), nullable=False)

    # books = relationship('Book', secondary='book_genre_detail', lazy='subquery',
    #                      backref=backref('genres', lazy=True))

    def __str__(self):
        return self.genre_detail


# book-book_genre's joining table
book_genre_detail = db.Table('book_genre_detail',
                             Column('id', Integer, primary_key=True, autoincrement=True),
                             Column('barcode', String(50), ForeignKey(Book.barcode)),
                             Column('genre_id', Integer, ForeignKey(Genre.genre_id)))


# class GenreDetail(db.Model):
#     barcode = Column(String(50), ForeignKey(Book.barcode), primary_key=True)
#     genre_id = Column(Integer, ForeignKey(Genre.genre_id), primary_key=True)


@dataclass
class Author(db.Model):
    __tablename__ = "author"

    author_id = Column(Integer, primary_key=True, autoincrement=True)
    author_name = Column(String(50), nullable=False)
    author_other_info = Column(String(255), nullable=True)

    # books = relationship('Book', secondary='author_detail', lazy='subquery',
    #                      backref=backref('authors', lazy=True))

    def __str__(self):
        return self.author_name


# book-author's joining table
author_detail = db.Table('author_detail',
                         Column('id', Integer, primary_key=True, autoincrement=True),
                         Column('barcode', String(50), ForeignKey(Book.barcode)),
                         Column('author_id', Integer, ForeignKey(Author.author_id)))


# class AuthorDetail(db.Model):
#     barcode = Column(String(50), ForeignKey(Book.barcode), primary_key=True)
#     author_id = Column(Integer, ForeignKey(Author.author_id), primary_key=True)


@dataclass
class Publisher(db.Model):
    __tablename__ = "publisher"

    publisher_id = Column(Integer, primary_key=True, autoincrement=True)
    publisher_name = Column(String(50), nullable=False)
    publisher_phone_number = Column(String(50), nullable=False)
    publisher_address = Column(String(50), nullable=False)

    # books = relationship('Book', secondary='publisher_detail', lazy='subquery',
    #                      backref=backref('publishers', lazy=True))

    def __str__(self):
        return self.publisher_name


# book-publisher's joining table
publisher_detail = db.Table('publisher_detail',
                            Column('id', Integer, primary_key=True, autoincrement=True),
                            Column('barcode', String(50), ForeignKey(Book.barcode)),
                            Column('publisher_id', Integer, ForeignKey(Publisher.publisher_id)))


class Customer(db.Model):
    __tablename__ = "customer"

    customer_id = Column(String(50), nullable=False, primary_key=True)
    customer_phone_number = Column(String(50), nullable=False)
    customer_name = Column(String(50), nullable=False)
    customer_email = Column(String(50), nullable=True)
    customer_address = Column(String(50), nullable=False)

    orders = relationship('SaleOrder', backref='customer', lazy=True)
    cash_bills = relationship('CashBill', backref='customer', lazy=True)
    debt_reports = relationship('DebtReport', backref="books", lazy=True)

    def __str__(self):
        return self.customer_name

    # def date_convert(self, value):
    #     return datetime(self.date_format(value))

    def to_json(self):
        return {
            "customer_id": self.customer_id,
            "customer_name": self.customer_name,
            "customer_email": self.customer_email,
            "customer_phone_number": self.customer_phone_number,
            "customer_address": self.customer_address,
        }

    def insert(self):
        try:
            db.session.add(self)
            db.session.commit()  # Apply transaction
            return True
        except Exception as ex:
            print(ex)
            db.session.rollback()  # Cancel transaction
        return False

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()  # Apply transaction
            return True
        except Exception as ex:
            print(ex)
            db.session.rollback()  # Cancel transaction
        return False


class SaleOrder(db.Model):
    __tablename__ = "sale_order"

    order_id = Column(Integer, primary_key=True, autoincrement=True)
    created = Column(DateTime, nullable=False, default=datetime.now())
    debt_value = Column(Float, nullable=True, default=0)
    total = Column(Float, nullable=False, default=0)

    customer_id = Column(String(50), ForeignKey(Customer.customer_id), nullable=True)
    creator_id = Column(String(50), ForeignKey(Employee.id), nullable=False)
    details = relationship('OrderDetail', backref='sale_order', lazy=True)

    def __str__(self):
        return str(self.order_id) + "/" + str(self.created)

    def insert(self):
        try:
            db.session.add(self)
            db.session.commit()  # Apply transaction
            return True
        except Exception as ex:
            print(ex)
            db.session.rollback()  # Cancel transaction
        return False


# book-sale_order's joining table
class OrderDetail(db.Model):
    id = Column(Integer, primary_key=True, autoincrement=True)

    order_id = Column(Integer, ForeignKey(SaleOrder.order_id))
    barcode = Column(String(50), ForeignKey(Book.barcode))
    sell_quantity = Column(Integer, nullable=False, default=0)
    unit_price = Column(Float, nullable=False, default=0)


class CashBill(db.Model):
    __tablename__ = "cash_bill"

    id = Column(Integer, primary_key=True, autoincrement=True)
    created = Column(DateTime, nullable=False, default=datetime.now())
    cash = Column(Float, nullable=False, default=0)

    customer_id = Column(String(50), ForeignKey(Customer.customer_id))
    creator_id = Column(String(50), ForeignKey(Employee.id))

    def __str__(self):
        return str(self.id) + "/" + str(self.created)

    def insert(self):
        try:
            db.session.add(self)
            db.session.commit()  # Apply transaction
            return True
        except Exception as ex:
            print(ex)
            db.session.rollback()  # Cancel transaction
        return False


class ReportType(enum.Enum):
    STOCK = "stock"
    DEBT = "debt"


class Report(db.Model):
    __tablename__ = "report"

    month_year = Column(DateTime, primary_key=True)
    created = Column(DateTime, nullable=False, default=datetime.now())
    report_type = Column(Enum(ReportType), default=ReportType.STOCK)
    creator_id = Column(String(50), ForeignKey(Manager.id), nullable=False)

    stock_details = relationship('StockReport', backref="report", lazy=True)
    debt_details = relationship('DebtReport', backref="report", lazy=True)


# report-book's joining table
class StockReport(db.Model):
    __tablename__ = "stock_report"

    id = Column(Integer, primary_key=True, autoincrement=True)
    month_year = Column(DateTime, ForeignKey(Report.month_year))
    barcode = Column(String(50), ForeignKey(Book.barcode), nullable=False)

    opening_stock = Column(Integer, nullable=True, default=0)
    in_stock_incurred = Column(Integer, nullable=True, default=0)
    closing_stock = Column(Integer, nullable=True, default=0)


# report-customer's joining table
class DebtReport(db.Model):
    __tablename__ = "debt_report"

    id = Column(Integer, primary_key=True, autoincrement=True)
    month_year = Column(DateTime, ForeignKey(Report.month_year))
    customer_id = Column(String(50), ForeignKey(Customer.customer_id))

    opening_stock = Column(Float, nullable=True, default=0)
    in_stock_incurred = Column(Float, nullable=True, default=0)
    closing_stock = Column(Float, nullable=True, default=0)


class ImportReceipt(db.Model):
    __tablename__ = "import_receipt"

    import_id = Column(Integer, primary_key=True, autoincrement=True)
    import_date = Column(DateTime, nullable=False, default=datetime.now())
    created = Column(DateTime, nullable=False, default=datetime.now())
    updated = Column(DateTime, nullable=False, default=datetime.now())
    details = relationship('ImportDetail', backref='import_receipt', lazy=True)

    def insert(self):
        try:
            db.session.add(self)
            db.session.commit()  # Apply transaction
            return True
        except Exception as ex:
            print(ex)
            db.session.rollback()  # Cancel transaction
        return False


class ImportDetail(db.Model):
    id = Column(Integer, primary_key=True, autoincrement=True)

    import_id = Column(Integer, ForeignKey(ImportReceipt.import_id))
    barcode = Column(String(50), ForeignKey(Book.barcode))
    pre_quantity = Column(Integer, nullable=False, default=0)
    import_quantity = Column(Integer, nullable=False, default=0)

    def __init__(self, import_id, barcode, pre_quantity, import_quantity):
        self.import_id = import_id
        self.barcode = barcode
        self.pre_quantity = pre_quantity
        self.import_quantity = import_quantity

    def __str__(self):
        return str(self.id) + " - " + self.barcode + " - " + str(self.import_quantity)

    def insert(self):
        try:
            db.session.add(self)
            db.session.commit()  # Apply transaction
            return True
        except Exception as ex:
            print(ex)
            db.session.rollback()  # Cancel transaction
        return False


if __name__ == "__main__":
    db.create_all()
