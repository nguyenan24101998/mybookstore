from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_login import LoginManager

my_app = Flask(__name__)
my_app.secret_key = "b'G\x96m\xf1\xde\xd8\x1f\xec\x95\xbf\x13E\xf3\xb4_\x83\x12.\xde\xf3'"

# Database Config
my_app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://root:2410@localhost/bookstore_db?charset=utf8mb4"
my_app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

db = SQLAlchemy(app=my_app)
admin = Admin(app=my_app, name="Hệ thống dành cho Quản Lý", template_mode="bootstrap3")
login = LoginManager(app=my_app)
