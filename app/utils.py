import csv
import os
from datetime import datetime

from flask import session

from app import my_app
from app.constant import *


def is_temp_order_created():
    return KEY_TEMP_ORDER_SESSION in session and len(session[KEY_TEMP_ORDER_SESSION]) > 0


def change_quantity_temp_books(index, quantity):
    session[KEY_TEMP_ORDER_SESSION][index]["quantity"] = quantity
    session[KEY_TEMP_ORDER_SESSION][index]["price"] = session[KEY_TEMP_ORDER_SESSION][index]["unit_price"] * quantity
    return session[KEY_TEMP_ORDER_SESSION]


def write_temp_order(book):
    # temp_books = read_temp_books()
    if is_temp_order_created():
        # Increase quantity if it's existing book
        for idx, temp_book in enumerate(session[KEY_TEMP_ORDER_SESSION]):
            if temp_book["barcode"] == book.barcode:
                return change_quantity_temp_books(index=idx, quantity=temp_book["quantity"] + 1)
    else:
        session[KEY_TEMP_ORDER_SESSION] = []

    session[KEY_TEMP_ORDER_SESSION].append(book.to_json())

    return session[KEY_TEMP_ORDER_SESSION]


def total_calculator():
    temp_order = session[KEY_TEMP_ORDER_SESSION]
    total = 0
    for temp_book in temp_order:
        total += temp_book["price"]
    return total


def debt_calculator(debt_value):
    if debt_value == 0:
        return 0
    return total_calculator() * float(debt_value) / 100


def total_debt_calculator(orders):
    total = 0.0
    for order in orders:
        total += order.debt_value
    return total


def remove_temp_book(barcode):
    # temp_order = session[KEY_TEMP_ORDER]
    if len(session[KEY_TEMP_ORDER_SESSION]) == 1:
        session[KEY_TEMP_ORDER_SESSION] = []
    else:
        for idx, book in enumerate(session[KEY_TEMP_ORDER_SESSION]):
            if book["barcode"].strip() == str(barcode).strip():
                del session[KEY_TEMP_ORDER_SESSION][idx]
                # if len(session[KEY_TEMP_ORDER]) == 0:
                #     session[KEY_TEMP_ORDER] = []
                break
    return session[KEY_TEMP_ORDER_SESSION]


def clear_temp():
    session[KEY_TEMP_ORDER_SESSION] = []
    return True


def export_csv(data, filename, fieldnames):
    file = os.path.join(my_app.root_path, "data/" + filename + "-%s.csv" % str(datetime.now()))
    with open(file, "w", encoding="utf-8") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()

        for d in data:
            writer.writerow(d)

    return file
