KEY_USER_SESSION = "user"
KEY_TEMP_ORDER_SESSION = "temp_order"
KEY_PRINT_BILL_SESSION = "print_bill"
KEY_IMPORT_SESSION = "temp_import"
KEY_CUSTOMER_SESSION = "temp_customer"
KEY_CASH_BILL_SESSION = "temp_cash_bill"

KEY_IMPORT_QUAN_RULE = "qd1"
KEY_IMPORT_IN_STOCK_RULE = "qd2"
KEY_CASH_BILL_RULE = "qd5"

KEY_SUCCESS_TITLE = "Thành công!"
KEY_ERROR_TITLE = "Đã có lỗi xảy ra!"
